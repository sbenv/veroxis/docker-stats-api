use color_eyre::eyre::eyre;
use color_eyre::Result;
use docker_stats_api::config::{TOKIO_THREADS, UPDATE_INTERVAL};
use docker_stats_api::{data_aggregator, docker_sys, sys, webserver};
use tokio::runtime::Builder;

fn main() -> Result<()> {
    init_logging();

    lazy_static::initialize(&TOKIO_THREADS);
    // Create the runtime
    let runtime = Builder::new_multi_thread()
        .worker_threads(*TOKIO_THREADS)
        .enable_io()
        .enable_time()
        .build()
        .map_err(|err| eyre!("failed to create tokio runtime: {err:?}"))?;

    // Spawn the root task
    runtime.block_on(async {
        run().await.unwrap();
    });

    Ok(())
}

fn init_logging() {
    #[cfg(debug_assertions)]
    {
        if std::env::var("RUST_LIB_BACKTRACE").is_err() {
            std::env::set_var("RUST_LIB_BACKTRACE", "1")
        }
        if std::env::var("RUST_BACKTRACE").is_err() {
            std::env::set_var("RUST_BACKTRACE", "full")
        }
    }

    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "info")
    }

    tracing_subscriber::fmt::init();
}

async fn run() -> Result<()> {
    color_eyre::install()?;

    lazy_static::initialize(&UPDATE_INTERVAL);

    tokio::join!(
        docker_sys::run(),
        sys::run(),
        data_aggregator::run(),
        webserver::run()
    );

    Ok(())
}

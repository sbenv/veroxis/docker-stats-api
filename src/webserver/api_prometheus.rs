use axum::http::StatusCode;
use axum::response::IntoResponse;
use color_eyre::Result;
use prometheus::{Registry, TextEncoder};

use crate::data_aggregator::get_metrics;
use crate::prometheus::PrometheusRegistrySource;

pub async fn api_prometheus() -> impl IntoResponse {
    let mut registry = Registry::new();

    let metrics = get_metrics().await;
    metrics.to_registry(&mut registry);

    match registry_to_string(&registry) {
        Ok(response) => (StatusCode::OK, response),
        Err(error) => {
            tracing::error!("failed to collect metrics: {}", error.to_string());
            (StatusCode::INTERNAL_SERVER_ERROR, String::new())
        }
    }
}

fn registry_to_string(registry: &Registry) -> Result<String> {
    let encoder = TextEncoder::new();
    let metric_families = registry.gather();
    let data = encoder.encode_to_string(&metric_families)?;
    Ok(data)
}

use axum::http::StatusCode;
use axum::response::IntoResponse;
use serde_json::json;

use crate::data_aggregator;

pub async fn api_json() -> impl IntoResponse {
    match data_aggregator::get_as_json().await {
        Ok(json) => (StatusCode::OK, json.to_string()),
        Err(error) => (
            StatusCode::INTERNAL_SERVER_ERROR,
            json!({"error": error.to_string()}).to_string(),
        ),
    }
}

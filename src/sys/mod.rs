use std::collections::HashMap;
use std::time::Duration;

use prometheus::{Gauge, Opts};
use serde::{Deserialize, Serialize};
use tokio::sync::Mutex;
use tokio::time::sleep;

use crate::config::UPDATE_INTERVAL;
use crate::prometheus::{gauge_set, PrometheusRegistrySource};

lazy_static::lazy_static!(
    static ref SYS_INFO_CACHE: Mutex<SysInfo> = Mutex::new(SysInfo::new());
);

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct SysInfo {
    os_name: Option<String>,
    os_kernel_version: Option<String>,
    os_version: Option<String>,
    os_host_name: Option<String>,

    cpus: usize,
    cpus_physical: Option<usize>,

    total_memory: u64,
    used_memory: u64,
    total_swap: u64,
    used_swap: u64,
}

impl Default for SysInfo {
    fn default() -> Self {
        let system = sysinfo::System::new_all();

        SysInfo {
            os_name: sysinfo::System::name(),
            os_kernel_version: sysinfo::System::kernel_version(),
            os_version: sysinfo::System::os_version(),
            os_host_name: sysinfo::System::host_name(),
            cpus: system.cpus().len(),
            cpus_physical: system.physical_core_count(),
            total_memory: system.total_memory(),
            used_memory: system.used_memory(),
            total_swap: system.total_swap(),
            used_swap: system.used_swap(),
        }
    }
}

impl SysInfo {
    pub fn new() -> Self {
        SysInfo::default()
    }
}

impl PrometheusRegistrySource for SysInfo {
    fn to_registry(&self, registry: &mut prometheus::Registry) {
        let mut opts = Opts::new("os_info", "os description data with a constant value of 1");
        opts = opts.const_label(
            "name",
            self.os_name
                .clone()
                .unwrap_or_else(|| "unknown".to_string()),
        );
        opts = opts.const_label(
            "kernel_version",
            self.os_kernel_version
                .clone()
                .unwrap_or_else(|| "unknown".to_string()),
        );
        opts = opts.const_label(
            "version",
            self.os_version
                .clone()
                .unwrap_or_else(|| "unknown".to_string()),
        );
        opts = opts.const_label(
            "host_name",
            self.os_host_name
                .clone()
                .unwrap_or_else(|| "unknown".to_string()),
        );
        match Gauge::with_opts(opts) {
            Ok(gauge) => {
                gauge.set(1.0);
                if let Err(error) = registry.register(Box::new(gauge)) {
                    tracing::warn!("failed to register os_info: {}", error);
                }
            }
            Err(error) => tracing::warn!("failed to create os_info gauge: {}", error),
        }
        gauge_set(
            registry,
            "os_total_memory",
            "total amount of available memory",
            HashMap::new(),
            self.total_memory as f64,
        );
        gauge_set(
            registry,
            "os_used_memory",
            "total amount of used memory",
            HashMap::new(),
            self.used_memory as f64,
        );
        gauge_set(
            registry,
            "os_total_swap",
            "total amount of available swap",
            HashMap::new(),
            self.total_swap as f64,
        );
        gauge_set(
            registry,
            "os_used_swap",
            "total amount of used swap",
            HashMap::new(),
            self.used_swap as f64,
        );
        gauge_set(
            registry,
            "os_logical_cpus",
            "amount of available logical cpu cores",
            HashMap::new(),
            self.cpus as f64,
        );
        if let Some(physical_cpus) = self.cpus_physical {
            gauge_set(
                registry,
                "os_physical_cpus",
                "amount of available physical cpu cores",
                HashMap::new(),
                physical_cpus as f64,
            );
        }
    }
}

pub async fn run() {
    lazy_static::initialize(&SYS_INFO_CACHE);
    loop {
        update_sys_metrics().await;
        sleep(Duration::from_secs(*UPDATE_INTERVAL)).await;
    }
}

async fn update_sys_metrics() {
    let sys_info = SysInfo::new();
    let mut sys_info_cache = (*SYS_INFO_CACHE).lock().await;
    (*sys_info_cache) = sys_info;
}

pub async fn get_metrics() -> SysInfo {
    (*SYS_INFO_CACHE).lock().await.clone()
}

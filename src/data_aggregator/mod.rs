pub mod docker_stats;

use serde::{Deserialize, Serialize};
use serde_json::Value;
use tokio::sync::Mutex;
use tokio::time::{sleep, Duration};

use self::docker_stats::DockerContainerStats;
use crate::config::UPDATE_INTERVAL;
use crate::docker_sys::get_docker_stat_entries;
use crate::prometheus::PrometheusRegistrySource;
use crate::sys;
use crate::sys::SysInfo;

lazy_static::lazy_static!(
    static ref METRICS_CACHE: Mutex<Metrics> = Mutex::new(Metrics::default());
);

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct Metrics {
    pub docker_stats: Vec<DockerContainerStats>,
    pub sys_info: SysInfo,
}

impl Metrics {
    pub fn to_value(&self) -> Result<Value, serde_json::Error> {
        serde_json::to_value(self)
    }
}

impl PrometheusRegistrySource for Metrics {
    fn to_registry(&self, registry: &mut prometheus::Registry) {
        self.docker_stats
            .iter()
            .for_each(|container| container.to_registry(registry));
        self.sys_info.to_registry(registry);
    }
}

pub async fn run() {
    lazy_static::initialize(&METRICS_CACHE);
    loop {
        tokio::join!(update_docker_metrics(), update_sys_metrics());
        sleep(Duration::from_secs(*UPDATE_INTERVAL)).await;
    }
}

async fn update_docker_metrics() {
    let docker_stats: Vec<DockerContainerStats> = get_docker_stat_entries()
        .await
        .iter()
        .map(DockerContainerStats::from)
        .collect();
    let mut metrics_cache = (*METRICS_CACHE).lock().await;
    metrics_cache.docker_stats = docker_stats;
}

async fn update_sys_metrics() {
    let sys_info = sys::get_metrics().await;
    let mut metrics_cache = (*METRICS_CACHE).lock().await;
    metrics_cache.sys_info = sys_info;
}

pub async fn get_as_json() -> Result<Value, serde_json::Error> {
    let metrics_cache = (*METRICS_CACHE).lock().await;
    (*metrics_cache).to_value()
}

pub async fn get_metrics() -> Metrics {
    (*METRICS_CACHE).lock().await.clone()
}

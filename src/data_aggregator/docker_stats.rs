use std::collections::HashMap;

use parse_size::parse_size;
use serde::{Deserialize, Serialize};

use crate::docker_sys::DockerStatEntry;
use crate::prometheus::{gauge_set, PrometheusRegistrySource};

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct DockerContainerStats {
    pub id: String,
    pub container: String,
    pub name: String,
    pub pids: u64,
    pub cpu_percent: f64,
    pub mem_percent: f64,
    pub mem_usage: MemUsage,
    pub block_io: BlockIO,
    pub net_io: NetIO,
}

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct BlockIO {
    pub read: u64,
    pub write: u64,
}

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct NetIO {
    pub sent: u64,
    pub received: u64,
}

#[derive(Default, Debug, Serialize, Deserialize, Clone)]
pub struct MemUsage {
    pub used: u64,
    pub available: u64,
}

impl From<&DockerStatEntry> for DockerContainerStats {
    fn from(docker_stat_entry: &DockerStatEntry) -> Self {
        let mut container_stats = Self::default();
        if let Some((read, write)) = docker_stat_entry.block_io.split_once(" / ") {
            if let Ok(read) = parse_size(read) {
                container_stats.block_io.read = read;
            }
            if let Ok(write) = parse_size(write) {
                container_stats.block_io.write = write;
            }
        }
        if let Some((cpu_percent, _)) = docker_stat_entry.cpu_perc.split_once('%') {
            if let Ok(cpu_percent) = cpu_percent.parse::<f64>() {
                container_stats.cpu_percent = cpu_percent;
            }
        }
        container_stats.container = docker_stat_entry.container.clone();
        container_stats.id = docker_stat_entry.id.clone();
        if let Some((mem_percent, _)) = docker_stat_entry.mem_perc.split_once('%') {
            if let Ok(mem_percent) = mem_percent.parse::<f64>() {
                container_stats.mem_percent = mem_percent;
            }
        }
        if let Some((used, available)) = docker_stat_entry.mem_usage.split_once(" / ") {
            if let Ok(used) = parse_size(used) {
                container_stats.mem_usage.used = used;
            }
            if let Ok(available) = parse_size(available) {
                container_stats.mem_usage.available = available;
            }
        }
        container_stats.name = docker_stat_entry.name.clone();
        if let Some((sent, received)) = docker_stat_entry.net_io.split_once(" / ") {
            if let Ok(sent) = parse_size(sent) {
                container_stats.net_io.sent = sent;
            }
            if let Ok(received) = parse_size(received) {
                container_stats.net_io.received = received;
            }
        }
        if let Ok(pids) = docker_stat_entry.pids.parse::<u64>() {
            container_stats.pids = pids;
        }
        container_stats
    }
}

impl PrometheusRegistrySource for DockerContainerStats {
    fn to_registry(&self, registry: &mut prometheus::Registry) {
        let mut container_identifiers = HashMap::new();
        container_identifiers.insert("name".to_string(), self.name.to_string());

        gauge_set(
            registry,
            "container_memory_usage_percent",
            "memory percent used by container",
            container_identifiers.clone(),
            self.mem_percent,
        );
        gauge_set(
            registry,
            "container_cpu_usage_percent",
            "cpu percent used by container",
            container_identifiers.clone(),
            self.cpu_percent,
        );
        gauge_set(
            registry,
            "container_memory_usage",
            "memory used by container",
            container_identifiers.clone(),
            self.mem_usage.used as f64,
        );
        gauge_set(
            registry,
            "container_memory_available",
            "memory available for container",
            container_identifiers.clone(),
            self.mem_usage.available as f64,
        );
        gauge_set(
            registry,
            "container_block_io_read",
            "block_io read value of container",
            container_identifiers.clone(),
            self.block_io.read as f64,
        );
        gauge_set(
            registry,
            "container_block_io_write",
            "block_io write value of container",
            container_identifiers.clone(),
            self.block_io.write as f64,
        );
        gauge_set(
            registry,
            "container_net_io_read",
            "net_io sent value of container",
            container_identifiers.clone(),
            self.net_io.sent as f64,
        );
        gauge_set(
            registry,
            "container_net_io_write",
            "net_io received value of container",
            container_identifiers.clone(),
            self.net_io.received as f64,
        );
    }
}

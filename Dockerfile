FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.20.2 as builder

COPY . /app
WORKDIR /app
RUN cp "target/$(uname -m)-unknown-linux-musl/release/docker-stats-api" "/usr/local/bin/docker-stats-api"

FROM registry.gitlab.com/sbenv/veroxis/images/alpine:3.20.2

RUN apk add --no-cache "dumb-init"

COPY --from=registry.gitlab.com/sbenv/veroxis/images/docker:27.1.2 "/usr/local/bin/docker" "/usr/local/bin/docker"
COPY --from=builder "/usr/local/bin/docker-stats-api" "/usr/local/bin/docker-stats-api"

ENTRYPOINT ["/usr/bin/dumb-init"]
CMD ["/usr/local/bin/docker-stats-api"]
